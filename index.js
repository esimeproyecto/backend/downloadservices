const express = require('express');
const app = express();
var uuid = require('uuid-random');
const bodyParser = require('body-parser');
var cors = require('cors');
var zip = require('bestzip');


const port = process.env.PORT || 3600;


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.post('/descarga', async function(req, res) {
    console.log(req.body.path);
    let absolutPath = req.body.path;
    await zip({
        source: `${absolutPath}/*`,
        destination: `${absolutPath}.zip`
    }).then(function() {
        console.log('all done!');
    }).catch(function(err) {
        console.error(err.stack);
        process.exit(1);
    });
    res.download(`${absolutPath}.zip`);
});

app.get('/hola', async function(req, res) {
    res.json("hola")
});

app.listen(port, function() {
    console.log('El sitio de APIs inició correctamente en el puerto: ', port);
});